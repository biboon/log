package log

import (
	"context"
	"io"

	"gitlab.com/biboon/log/internal/core"
	"gitlab.com/biboon/log/internal/log"
)

type Logger = log.Logger

func New(w io.Writer, pretty, debug bool) Logger { return core.New(w, pretty, debug) }
func Nop() Logger                                { return core.Nop() }

// Storing & retrieving from contexts

type (
	ctxKeyLogger struct{}
	ctxKeyCorrID struct{}
)

func FromContext(ctx context.Context) Logger {
	if l, ok := ctx.Value(ctxKeyLogger{}).(Logger); ok {
		return l
	}

	return Nop()
}

func ContextWith(ctx context.Context, l Logger) context.Context {
	return context.WithValue(ctx, ctxKeyLogger{}, l)
}

func CorrelationIDFromContext(ctx context.Context) string {
	if id, ok := ctx.Value(ctxKeyCorrID{}).(string); ok {
		return id
	}

	return ""
}

func ContextWithCorrelationID(ctx context.Context, id string) context.Context {
	return context.WithValue(ctx, ctxKeyCorrID{}, id)
}
