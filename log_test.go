package log_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/biboon/log"
)

func TestMiddlewares(t *testing.T) {
	const correlationID = "deadb33f"

	b := bytes.Buffer{}
	l := log.New(&b, false, false)
	w := httptest.NewRecorder()
	r := httptest.NewRequest("PUT", "/target", http.NoBody)

	var h http.Handler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if id := log.CorrelationIDFromContext(r.Context()); id != correlationID {
			t.Fatal("expected", correlationID, "got", id)
		}

		w.WriteHeader(http.StatusTeapot)
	})

	h = log.LogHTTPRequestMiddleware()(h)
	h = log.SetCorrelationIDMiddleware(func() string { return correlationID })(h)
	h = log.SetLoggerMiddleware(l)(h)

	h.ServeHTTP(w, r)

	if id := w.Result().Header.Get("correlation-id"); id != correlationID {
		t.Fatal("expected", correlationID, "got", id)
	}

	var log map[string]any
	if err := json.NewDecoder(&b).Decode(&log); err != nil {
		t.Fatal(err)
	}

	for field, expected := range map[string]any{
		"id": "deadb33f", "proto": "HTTP/1.1", "method": "PUT", "path": "/target", "code": 418., "time": log["time"],
	} {
		switch actual, exists := log[field]; {
		case !exists:
			t.Fatal("field", field, "not found")
		case actual != expected:
			t.Fatal("expected", expected, "got", actual)
		}
	}
}
