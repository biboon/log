package log

import (
	"net/http"
	"time"

	"gitlab.com/biboon/log/internal/utils"
)

func SetLoggerMiddleware(l Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := ContextWith(r.Context(), l)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

const CorrelationIDHeaderKey = "correlation-id"

func SetCorrelationIDMiddleware(newid func() string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			id := r.Header.Get(CorrelationIDHeaderKey)
			if id == "" {
				id = newid()
			}

			ctx := r.Context()
			ctx = ContextWith(ctx, FromContext(ctx).WithFields("id", id))
			ctx = ContextWithCorrelationID(ctx, id)

			w.Header().Set(CorrelationIDHeaderKey, id)
			next.ServeHTTP(w, r.WithContext(ctx))
		}

		return http.HandlerFunc(fn)
	}
}

func LogHTTPRequestMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			rec, now := utils.NewResponseCodeRecorder(w), time.Now()
			next.ServeHTTP(rec, r)
			FromContext(r.Context()).LogHTTPRequest(r, rec.Code, time.Since(now))
		}

		return http.HandlerFunc(fn)
	}
}
